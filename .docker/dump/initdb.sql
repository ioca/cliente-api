CREATE DATABASE IF NOT EXISTS ClienteDB;

USE ClienteDB;

CREATE TABLE IF NOT EXISTS Cliente (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(200) NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    nascimento DATE NOT NULL
);

INSERT INTO Cliente (nome, cpf, nascimento) VALUES ("João De Santo Cristo", "33344455523", "2018-07-21");
INSERT INTO Cliente (nome, cpf, nascimento) VALUES ("Maria Lucia", "33344455523", "2018-07-22");
INSERT INTO Cliente (nome, cpf, nascimento) VALUES ("Pablo", "33344455523", "2018-07-23");
INSERT INTO Cliente (nome, cpf, nascimento) VALUES ("Jeremias", "33344455523", "2018-07-24");

SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;
