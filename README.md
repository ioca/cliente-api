# Cliente API!

## Requerimentos

Para executar a aplicação é necessário:

## Requerimentos

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- [Docker](https://www.docker.com/products/docker-desktop)

## Tecnologias

* Java
* Spring 
* Spring Boot
* mySql
* docker

# Desenvolvimento

## Executando a aplicação localmente
1. Acessar o diretório .docker
2. Executar o docker-compose up -d para iniciar o container mySql
3. Iniciar o projeto SpringBoot
4. Utilizar o arquivo dentro do diretorio postman-collection para consumir a api
