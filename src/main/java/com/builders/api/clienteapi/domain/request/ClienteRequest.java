package com.builders.api.clienteapi.domain.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ClienteRequest {

    @NotNull(message = "nome é obrigatório")
    private String nome;

    @Pattern(regexp = "^[0-9]{11}$", message = "Parâmetro 'cpf' deve conter 11 caracteres")
    @NotNull(message = "cpf é obrigatório")
	private String cpf;

	@Pattern(regexp = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))",
    message = "A data de início deve estar no formato yyyy-MM-dd")
    @NotNull(message = "Nascimento é obrigatório")
    private String nascimento;
}