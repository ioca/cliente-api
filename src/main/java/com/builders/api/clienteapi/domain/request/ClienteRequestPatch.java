package com.builders.api.clienteapi.domain.request;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ClienteRequestPatch {

    private String nome;

    @Pattern(regexp = "^[0-9]{11}$", message = "Parâmetro 'cpf' deve conter 11 caracteres")
	private String cpf;

	@Pattern(regexp = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))",
    message = "A data de início deve estar no formato yyyy-MM-dd")
    private String nascimento;

}