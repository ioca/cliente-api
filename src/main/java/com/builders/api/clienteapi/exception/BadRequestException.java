package com.builders.api.clienteapi.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String message;
	private Throwable throwable;
    private HttpStatus status;
    
    public BadRequestException(HttpStatus status, Throwable cause) {
		super(cause);
		this.status = status;
	}

	public BadRequestException(HttpStatus status, String message) {
		super(message);
		this.message = message;
		this.status = status;
	}
}