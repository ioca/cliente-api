package com.builders.api.clienteapi.exception;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import static java.lang.String.format;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final String MENSAGEM_GLOBAL_500 = "Erro interno do sistema.";
    public static final String FALHA_NO_REQUEST_MSG_PATTERN = "Falha no request: Objeto - [%s] Campo - [%s] Valor - [%s]";
    public static final String FALHA_PARAMETRO_FORMATO_INVALIDO = "Parâmetro '%s' aceita apenas números - Valor [%s]";

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorInfo> handleExceptionServerError(HttpServletRequest request, Exception ex) {
                   
         String message = MENSAGEM_GLOBAL_500;
         HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

         return new ResponseEntity<>(buildErrorInfo(request, ex, singletonList(message)), httpStatus);
    }

     @ResponseStatus(HttpStatus.BAD_REQUEST)
     @ExceptionHandler(BadRequestException.class)
     @ResponseBody
     public ErrorInfo handleBadRequestExceptionError(HttpServletRequest request, Exception ex) {

        return buildErrorInfo(request, ex, singletonList(ex.getMessage()));
     }

     @ResponseStatus(HttpStatus.BAD_REQUEST)
     @ExceptionHandler(MethodArgumentNotValidException.class)
     @ResponseBody
     public ErrorInfo handleMethodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException ex) {

        final List<String> messages = new ArrayList<>();
          ofNullable(ex.getBindingResult().getAllErrors()).orElse(emptyList()).forEach(objectError ->
               messages.add(buildMethodArgumentNotValidExceptionErrors(objectError)));

          return buildErrorInfo(request, ex, messages);
     }

     public ErrorInfo buildErrorInfo(HttpServletRequest request, Exception exceptionCdt, List<String> messages) {
        return ErrorInfo.builder().timestamp(LocalDateTime.now())
                .messages(messages)
                .exception(exceptionCdt.getClass().getSimpleName())
                .path(request.getRequestURI())
                .build();
    }

    public static String buildMethodArgumentNotValidExceptionErrors(ObjectError objectError){
        String defaultMsg = objectError.getDefaultMessage();
        if(!StringUtils.isEmpty(defaultMsg)){
            return defaultMsg;
        } else {
            if (objectError instanceof FieldError) {
                final FieldError field = (FieldError) objectError;
                return format(FALHA_NO_REQUEST_MSG_PATTERN, field.getObjectName(), field.getField(), field.getRejectedValue());
            } else {
                return format(FALHA_NO_REQUEST_MSG_PATTERN, objectError.getObjectName(), objectError.getArguments(), objectError.getCodes());
            }
        }
    }

}