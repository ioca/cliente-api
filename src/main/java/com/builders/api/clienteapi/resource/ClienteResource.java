package com.builders.api.clienteapi.resource;

import javax.validation.Valid;

import com.builders.api.clienteapi.domain.request.ClienteRequest;
import com.builders.api.clienteapi.domain.request.ClienteRequestPatch;
import com.builders.api.clienteapi.domain.response.ClienteResponse;
import com.builders.api.clienteapi.entity.Cliente;
import com.builders.api.clienteapi.service.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/clientes")
public class ClienteResource {

    @Autowired
    private ClienteService service;

	@GetMapping("/{id}")
	public ResponseEntity<ClienteResponse> buscar(@PathVariable final Long id) {

        return ResponseEntity.ok(service.buscar(id));
    }

    @GetMapping
    public ResponseEntity<Page<Cliente>> listar(
            @RequestParam(name = "nome", defaultValue = "") final String nome,
            @RequestParam(name = "cpf", defaultValue = "") final String cpf, 
            final Pageable pageable) {

        final Cliente cliente = Cliente.builder().nome(nome).cpf(cpf).build();
        final Page<Cliente> clientes = service.listar(cliente, pageable);
		return ResponseEntity.ok(clientes);
    }
    
    @PostMapping
	public ResponseEntity<Void> salvar(@RequestBody @Valid ClienteRequest clienteRequest) {

        service.salvar(clienteRequest);
		return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    
    @DeleteMapping("/{id}")
	public ResponseEntity<Void> deletar(@PathVariable Long id) {
        
        service.deletar(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @PutMapping("/{id}")
	public ResponseEntity<Cliente> atualizar(@PathVariable Long id, @RequestBody @Valid ClienteRequest clienteRequest) {

		return ResponseEntity.ok(service.atualizar(id, clienteRequest));
    }
    
    @PatchMapping("/{id}")
	public ResponseEntity<Cliente> atualizarParcial(@PathVariable Long id, @RequestBody @Valid ClienteRequestPatch clienteRequestPatch) {

		return ResponseEntity.ok(service.atualizarParcial(id, clienteRequestPatch));
	}
}
