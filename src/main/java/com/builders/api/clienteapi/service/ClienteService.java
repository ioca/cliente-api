package com.builders.api.clienteapi.service;

import com.builders.api.clienteapi.domain.request.ClienteRequest;
import com.builders.api.clienteapi.domain.request.ClienteRequestPatch;
import com.builders.api.clienteapi.domain.response.ClienteResponse;
import com.builders.api.clienteapi.entity.Cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClienteService {

	ClienteResponse buscar(Long id);
	
	Page<Cliente> listar(Cliente cliente, Pageable pageable);

	void salvar(ClienteRequest clienteRequest);

	Cliente atualizar(Long id, ClienteRequest clienteRequest);
	
	Cliente atualizarParcial(Long id, ClienteRequestPatch clienteRequestPatch);

	void deletar(Long id);
}