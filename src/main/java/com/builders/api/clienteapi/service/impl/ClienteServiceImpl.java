package com.builders.api.clienteapi.service.impl;

import java.time.LocalDate;

import com.builders.api.clienteapi.domain.request.ClienteRequest;
import com.builders.api.clienteapi.domain.request.ClienteRequestPatch;
import com.builders.api.clienteapi.domain.response.ClienteResponse;
import com.builders.api.clienteapi.entity.Cliente;
import com.builders.api.clienteapi.exception.BadRequestException;
import com.builders.api.clienteapi.repository.ClienteRepository;
import com.builders.api.clienteapi.service.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.Data;

@Data
@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private final ClienteRepository repository;

    @Override
    public ClienteResponse buscar(Long id) {

        Cliente cliente = repository.findById(id).orElseThrow(() -> 
        new BadRequestException(HttpStatus.BAD_REQUEST, "Conta não encontrada para o identificador solicitado"));

        ClienteResponse retorno = 
            ClienteResponse.builder()
            .id(cliente.getId())
            .nome(cliente.getNome())
            .cpf(cliente.getCpf())
            .nascimento(cliente.getNascimento())
            .build();

        retorno.setIdade(retorno.getIdade());

        return retorno;
    }

    @Override
    public Page<Cliente> listar(Cliente cliente, Pageable pageable) {

        ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase().withStringMatcher(StringMatcher.CONTAINING);
        Example<Cliente> exemplo = Example.of(cliente, matcher);
        return repository.findAll(exemplo, pageable);
    }

    @Override
    public void salvar(ClienteRequest clienteRequest) {

        repository.saveAndFlush(Cliente.builder()
            .nome(clienteRequest.getNome())
            .cpf(clienteRequest.getCpf())
            .nascimento(LocalDate.parse(clienteRequest.getNascimento()))
            .build());
    }

    @Override
    public void deletar(Long id) {

        repository.findById(id).orElseThrow(() -> new RuntimeException());
        repository.deleteById(id);
    }

    @Override
    public Cliente atualizar(Long id, ClienteRequest clienteRequest) {

        Cliente cliente = repository.findById(id).orElseThrow(() -> new RuntimeException());

        return repository.saveAndFlush(Cliente
        .builder()
        .id(cliente.getId())
        .nome(clienteRequest.getNome())
        .cpf(clienteRequest.getCpf())
        .nascimento(LocalDate.parse(clienteRequest.getNascimento()))
        .build());
    }

    @Override
    public Cliente atualizarParcial(Long id, ClienteRequestPatch clienteRequestPatch) {

        Cliente cliente = repository.findById(id).orElseThrow(() -> new RuntimeException());

        return repository.saveAndFlush(Cliente
            .builder()
            .id(cliente.getId())
            .nome(clienteRequestPatch.getNome() == null ? cliente.getNome() : clienteRequestPatch.getNome())
            .cpf(clienteRequestPatch.getCpf() == null ? cliente.getCpf() : clienteRequestPatch.getCpf())
            .nascimento(
                clienteRequestPatch.getNascimento() == null 
                ? cliente.getNascimento()
                : LocalDate.parse(clienteRequestPatch.getNascimento()))
            .build());
    }
}