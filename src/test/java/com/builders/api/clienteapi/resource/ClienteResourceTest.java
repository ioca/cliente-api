package com.builders.api.clienteapi.resource;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MultiValueMap;

import com.builders.api.clienteapi.exception.BadRequestException;
import com.builders.api.clienteapi.exception.GlobalExceptionHandler;
import com.builders.api.clienteapi.service.ClienteService;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(ClienteResource.class)
public class ClienteResourceTest {
  
  @InjectMocks
  private ClienteResource clienteResource;
  
  @Mock
  private ClienteService clienteService;
  
  protected MockMvc mockMvc;
  protected ResultActions result;
  protected MvcResult mvcResult;
  protected MultiValueMap<String, String> requestParam;
  protected Map<String, Object> errorValues;
  
  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders
    .standaloneSetup(clienteResource)
    .setControllerAdvice(new GlobalExceptionHandler())
    .build();
  }
  
  @Test
  public void buscar_cliente_id_ok() throws Exception {
    
    mockMvc.perform(get("/api/clientes/1")
    .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk());
  }
  
  @Test
  public void buscar_cliente_id_400() throws Exception {
    
    when(clienteService.buscar(anyLong())).thenThrow(BadRequestException.class);
    
    mockMvc.perform(get("/api/clientes/1")
    .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isBadRequest());
  }
  
  public void listar_cliente_ok() {
    
    //TODO - Implementar metodo
  }
  
  public void listar_cliente_400() {
    
    //TODO - Implementar metodo 
  }
  
  public void deletar_cliente_ok() {
    
    //TODO - Implementar metodo 
  }
  
  public void deletar_cliente_400() {
    
    //TODO - Implementar metodo 
  }
  
  public void atualizar_cliente_200() {
    
    //TODO - Implementar metodo 
  }
  
  public void atualizar_cliente_400() {
    
    //TODO - Implementar metodo 
  }
  
  public void atualizar_parcial_cliente_200() {
    
    //TODO - Implementar metodo 
  }
  
  public void atualizar_parcial_cliente_400() {
    
    //TODO - Implementar metodo 
  }
}