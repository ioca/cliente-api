package com.builders.api.clienteapi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.builders.api.clienteapi.domain.response.ClienteResponse;
import com.builders.api.clienteapi.entity.Cliente;
import com.builders.api.clienteapi.exception.BadRequestException;
import com.builders.api.clienteapi.repository.ClienteRepository;
import com.builders.api.clienteapi.service.impl.ClienteServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ClienteServiceTest {
	
	@InjectMocks
	private ClienteServiceImpl clienteService;
	
	@Mock
	private ClienteRepository clienteRepository;
	
	@Test
	public void buscar_cliente_id_ok() {
		
		Cliente cliente = Cliente.builder().id(1L).nascimento(LocalDate.now()).build();
		when(clienteRepository.findById(anyLong()))
		.thenReturn(Optional.of(cliente));
		
		ClienteResponse response = clienteService.buscar(1L);
		
		assertThat(response.getId()).isEqualTo(cliente.getId());
	}
	
	@Test(expected = BadRequestException.class)
	public void buscar_cliente_id_400() {
		
		when(clienteRepository.findById(anyLong()))
		.thenReturn(Optional.empty());
		
		clienteService.buscar(1L); 
	}
	
	public void listar_cliente_ok() {
		
		//TODO - Implementar metodo
	}
	
	public void listar_cliente_400() {
		
		//TODO - Implementar metodo 
	}
	
	public void deletar_cliente_ok() {
		
		//TODO - Implementar metodo 
	}
	
	public void deletar_cliente_400() {
		
		//TODO - Implementar metodo 
	}
	
	public void atualizar_cliente_200() {
		
		//TODO - Implementar metodo 
	}
	
	public void atualizar_cliente_400() {
		
		//TODO - Implementar metodo 
	}
	
	public void atualizar_parcial_cliente_200() {
		
		//TODO - Implementar metodo 
	}
	
	public void atualizar_parcial_cliente_400() {
		
		//TODO - Implementar metodo 
	}
}
